import React, { useState } from 'react';
import Boton from './Boton';
import Display from './Display';


export default () => {
    const [display, setDisplay] = useState('');
    const [operador, setOperador] = useState('');
    const [valor, setValor] = useState('');
    const [miniDisplay, setMiniDisplay] = useState('');

    let nums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    let oper = ['+', '-', '*', '/'];

    const pulsaTecla = (e) => {

        if (nums.includes(e)) {

            setDisplay(display + e);
            setMiniDisplay(miniDisplay + e);

        } else if (oper.includes(e)) {

            setOperador(e);
            setValor(display);
            setDisplay('');
            setMiniDisplay(miniDisplay + e);

        } else if (e === '=') {

            if (operador === '+') {

                setDisplay(valor * 1 + display * 1);
                setMiniDisplay(miniDisplay + e + parseInt(valor * 1 + display * 1));

            } else if (operador === '-') {

                setDisplay(valor * 1 - display * 1);
                setMiniDisplay(miniDisplay + e + parseInt(valor * 1 - display * 1));

            } else if (operador === '/') {

                setDisplay(valor * 1 / display * 1);
                setMiniDisplay(miniDisplay + e + parseInt(valor * 1 / display * 1));

            } else if (operador === '*') {

                setDisplay(valor * 1 * display * 1);
                setMiniDisplay(miniDisplay + e + parseInt(valor * 1 * display * 1));
            }
            
        } else if (e === 'c') {
            setValor('');
            setOperador('');
            setDisplay('');
            setMiniDisplay('')
        }
    };



    return (
        <>
            <h1>Calculadora ReactJs</h1>
            
            <div>
                <Display valor={display} miniDisplay={miniDisplay} />
                <Boton valor="1" pulsa={pulsaTecla} />
                <Boton valor="2" pulsa={pulsaTecla} />
                <Boton valor="3" pulsa={pulsaTecla} />
                <Boton valor="*" pulsa={pulsaTecla} />
            </div>

            <div>
                <Boton valor="4" pulsa={pulsaTecla} />
                <Boton valor="5" pulsa={pulsaTecla} />
                <Boton valor="6" pulsa={pulsaTecla} />
                <Boton valor="/" pulsa={pulsaTecla} />
            </div>

            <div>
                <Boton valor="7" pulsa={pulsaTecla} />
                <Boton valor="8" pulsa={pulsaTecla} />
                <Boton valor="9" pulsa={pulsaTecla} />
                <Boton valor="-" pulsa={pulsaTecla} />
            </div>

            <div>
                <Boton valor="0" pulsa={pulsaTecla} />
                <Boton valor="c" pulsa={pulsaTecla} />
                <Boton valor="=" pulsa={pulsaTecla} />
                <Boton valor="+" pulsa={pulsaTecla} />
            </div>
        </>
    );
};