import React, { useState, useEffect } from "react";
import { v4 as uuid } from "uuid";
import {
    Button, Card, CardImg, CardText, CardBody,
    CardTitle, Input,
} from 'reactstrap';


const Items = () => {
    const [items, setItems] = useState([]);
    const [url, setUrl] = useState();
    const [comentario, setComentario] = useState();


    const guardar = () => {
        localStorage.setItem('mis_items', JSON.stringify(items));
    }

    const recuperar = () => {
        const itemsJson = localStorage.getItem('mis_items');
        const cosas = JSON.parse(itemsJson);
        if (cosas && cosas.length) {
            setItems(cosas);
        } else {
            setItems([]);
        }

    }

    useEffect(() => {
        if(items.length){
            guardar();
        }
      }, [items]);

    useEffect(() => {
        recuperar();
      }, []);

    const añadir =() => {
        if (url) {
            const nouItem = {
                imagen: url,
                id: uuid(),
                coment: comentario,
                likes: 0,
            };
            setItems([...items, nouItem]);
            setUrl('');
            setComentario('');
        }

    };

    const modifica = (id) => {
        if(id){
            const itemLikes = items.map((el) => {
                if(el.id === id){
                    el.likes = el.likes + 1;
                }
                return el;
            })
            setItems(itemLikes);
        }
    }

const tots = items.map((el) => (
    <div className="col-4" key={el.id}>
        <Card>
            <CardImg top width="100%" src={el.imagen} alt="Card image cap" />
            <CardBody>
                <CardTitle>{el.coment}</CardTitle>
                <CardText>Likes: {el.likes}</CardText>
                <Button color="danger" onClick={() => modifica(el.id)} ><i className="fa fa-heart" aria-hidden="true"></i></Button>
            </CardBody>
        </Card>
    </div>

));

return (
    <>
        <h1>InstaMotos</h1>
        <div className="container">
            <div className="row">
                <div className="col-3"></div>
                <div className="col-6">
                    <Input className="m-1" type="text" value={url} onChange={(ev) => setUrl(ev.target.value)} /> 
                    <Input className="m-1" type="text" value={comentario} onChange={(ev) => setComentario(ev.target.value)} /><br />
                    <Button className="m-1" color="success" onClick={añadir}>Añadir</Button>
                </div>
                <div className="col-3"></div>
            </div>
            <div className="row mt-3">
                {tots}
            </div>
        </div>
    </>
);
};

export default Items;
