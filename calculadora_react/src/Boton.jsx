import React from 'react';
import { Button } from 'reactstrap';

export default(props) => {

    return(
        <>
            <Button className="m-1" onClick={() => props.pulsa(props.valor)}>{props.valor}</Button>
        </>
    );
};