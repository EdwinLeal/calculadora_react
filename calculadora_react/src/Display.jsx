import React from 'react';
import { Alert } from 'reactstrap';

export default (props) => {

    return (
        <>
            <Alert color="secondary" 
            style={{height: "50px", width: "12%", position: "relative", left: "44%", textAlign: "right" }} >
                {props.valor}
                <Alert color="info"
                style={{padding:"1px", margin:"0", height: "20px", width: "165px", position: "absolute", left: "-1%", top:"80%", textAlign: "center" }}>
                    {props.miniDisplay}
                </Alert>
            </Alert>
        </>
    );
};